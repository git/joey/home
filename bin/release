#!/bin/bash
# Commit, tag, and release.
set -e

if [ -e CHANGELOG ]; then
	changelog=CHANGELOG
else
	if [ -d debian ]; then
		changelog=debian/changelog
	else
		if [ -e CHANGELOG.md ]; then
			changelog=CHANGELOG.md
		else
			echo "no debian/changelog or CHANGELOG or CHANGELOG.md found" >&2
			exit 1
		fi
	fi
fi

if head -1 $changelog | grep -q UNRELEASED; then
	echo "Changelog says it's UNRELEASED, bud."
	exit 1
fi

debversion=`dpkg-parsechangelog -l$changelog 2>/dev/null | grep Version: | cut -f 2 -d ' ' | sed s/[0-9]*://`
if [ "$debversion" != "unknown" ]; then
	# This makes the release tag.
	# It will exit nonzero if no changes need to be committed
	# (but still creates the tag).
	debcommit -c $changelog -a --release
else
	version=$(head -n 1 $changelog | grep '^#### ' | sed 's/^#### //')
	if [ -z "$version" ]; then
		echo "unable to determine version in $changelog"
		exit 1
	fi
	git tag -s "$version" -m "tagging release $version"
fi

if [ -e .git ]; then
	git push || true
	git push --tags || true
fi

wikiannounce .

# store debian package
if [ -d debian ]; then
	# try to remove past versions from build dir
	rm -f ${package}_*.changes ${package}_*.tar.gz ${package}_*.deb
fi

# release tarball to hackage
OK=
while [ -z "$OK" ]; do
	if [ -n "$(find -maxdepth 1 -name \*.cabal)" ]; then
		cabal sdist
		cabal upload --publish dist-newstyle/sdist/*.tar.gz && rm -f dist-newstyle/sdist/*.tar.gz && OK=1
	else
		echo "git-only release"
		OK=1
	fi
	if [ -z "$OK" ]; then
		echo "UPLOAD FAILED; press enter to retry"
		read i
	fi
done
