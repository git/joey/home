#!/usr/bin/perl
my $nocommit=0;
my $sourcedir=shift;
if (! length $sourcedir) {
	$sourcedir=".";
	$nocommit=1;
}

my $newsdir=`mr --minimal announcedir 2>/dev/null | tail -n 2| head -n 1`;
chomp $newsdir;
print "$newsdir\n";
if (! length $newsdir || ! -d $newsdir) {
	exit 0;
}

my $version;
my $source;
my $closes;
my $changes;
foreach my $f ("CHANGELOG", "debian/changelog") {
	if (-e "$sourcedir/$f") {
		$version=`dpkg-parsechangelog -l $f -S Version`;
		chomp $version;
		$source=`dpkg-parsechangelog -l $f -S Source`;
		chomp $source;
		$closes=`dpkg-parsechangelog -l $f -S Closes`;
		$changes=`dpkg-parsechangelog -l $f -S Changes`;
		my @changes=split '\n', $changes;
		@changes=grep /^\s/, @changes;
		$changes=join "\n", @changes;
		last;
	}
}

my $news_version;
my $news_closes;
my $news_changes;
foreach my $f ("NEWS", "debian/news") {
	if (-e "$sourcedir/$f") {
		$news_version=`dpkg-parsechangelog -l $f -S Version`;
		chomp $news_version;
		$news_closes=`dpkg-parsechangelog -l $f -S Closes`;
		$news_changes=`dpkg-parsechangelog -l $f -S Changes`;
		last;
	}
}

chdir($newsdir) || die "chdir $newsdir: $!\n";

my $file="version_$version.mdwn";

if (-e $file) {
	print "Wiki announcement already seems to exist, not overwriting.\n";
}
else {
	print "Generating wiki announcement...\n";

	open (OUT, ">$file") || die "write $file: $!";
	
	if ($news_version eq $version) {
		print OUT "News for $source $version:";
		print OUT "\n";
		print OUT mungechanges($news_changes, $news_closes);
		print OUT "\n\n";
	}

	print OUT "$source $version released with [[!toggle text=\"these changes\"]]\n";
	print OUT '[[!toggleable text="""';
	print OUT mungechanges($changes, $closes);
	print OUT '"""]]';
	close OUT;
}
system("git", "add", $file);

# Limit number of news items that are just version releases.
my $count=5;
foreach my $item (reverse sort vercmp glob("version_*")) {
	$count--;
	if ($count < 0 && $item ne $file) {
		system("git", "rm", $item);
	}
}
if (! $nocommit) {
	system("git", "commit", "-a", "-m", "add news item for $source $version");
	if (system("mr", "push") != 0) {
		if (system("mr", "update") == 0) {
			system("mr", "push");
		}
	}
}

sub mungechanges {
	my $changes=shift;
	my $closes=shift;
	$changes=~s/^ [^ ].*\n//gm;
	$changes=~s/^   \[/ * [/gm; # let markdown treat [ name ] as part of list
	$changes=~s/_/\\_/g; # markdown _ sillyness
	$changes=~s/\&/&amp;/g;
	$changes=~s/</&lt;/g;
	$changes=~s/>/&gt;/g;
	foreach my $bug (split ' ', $closes) {
		$changes=~s!\b$bug\b![$bug](http://bugs.debian.org/$bug)!g;
	}
	return $changes;
}

sub vercmp {
	my @abits=split(/\./, $a);
	my @bbits=split(/\./, $b);
	while (@abits && @bbits) {
		my $abit=shift @abits;
		my $bbit=shift @bbits;
		if ($abit != $bbit) {
			return $abit <=> $bbit;
		}
	}
	return $#abits <=> $#bbits;
}
